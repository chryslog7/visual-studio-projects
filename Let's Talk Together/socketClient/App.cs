﻿using AForge.Video.DirectShow;
using NAudio.Wave;
using NetFwTypeLib;
using PcapDotNet.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace p2pchat
{
    public partial class App : Form
    {

        public Intro formIntro;
        public static string Inputkey = "560A18CD-6346-4CF0-A2E8-671F9B6B9EA9";//make generator of this
        public int portNum;
        const int keySize = 2048;
        public bool isResp = false;

        public TcpListener myList;
        public Socket socket;
        public IPAddress tempIP;

        volatile bool AESexchange = false;
        volatile bool nodeA = false;
        volatile bool nodeB = false;
        volatile string recipientKey = null;
        volatile string aesKey = null;
        volatile bool startCall = false;

        int portBoxVal;
        public string string2send = "";
        public string nickName;
        string publicAndPrivateKey;
        string publicKey = null;
        string fileName = null;
        bool proxyEnabled = false;
        public byte[] fileBytes = null;
        byte[] data = new byte[2048];
        byte[] callBytes = new byte[2048];
        Stopwatch stopWatch = new Stopwatch();

        UdpClient udpServer;
        IPAddress ipBoxAddr;
        UTF8Encoding utf8 = new UTF8Encoding();
        ASCIIEncoding ascii = new ASCIIEncoding();
        static Thread ipThread, udpListen;
        Thread callThr;

        private FilterInfoCollection webcam;
        private VideoCaptureDevice cam;
        WaveInEvent sourceStream;
        WaveFileWriter waveWriter;
        string FilePath = @"C:/Records", FileName = "/record.wav";
        int InputDeviceIndex;

        //Mouse drag stuff begin//
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        //Mouse drag stuff end//


        public App(Intro formIntro)
        {
            InitializeComponent();
            this.formIntro = formIntro;
        }

        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            using (DeflateStream dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(data, 0, data.Length);
            }
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream(data);
            MemoryStream output = new MemoryStream();
            using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }
            return output.ToArray();
        }

        bool validIPandPort()
        {
            if (string.IsNullOrWhiteSpace(ipBox.Text) || !IPAddress.TryParse(ipBox.Text, out ipBoxAddr) || !ipBox.Text.Contains("."))
            {
                MessageBox.Show("Wrong IPv4 address", "App Error - Let's Talk Together");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(portBox.Text) || !int.TryParse(portBox.Text, out portBoxVal) || portBoxVal < 0 || portBoxVal > 65535)
            {
                MessageBox.Show("Wrong port", "App Error - Let's Talk Together");
                return false;
            }
            return true;
        }

        bool verifyXMLstring(string xmlString)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(xmlString);
                XmlNode nodeToFind;
                XmlElement root = doc.DocumentElement;
                nodeToFind = root.SelectSingleNode("/RSAKeyValue");
                if (nodeToFind != null) return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        void listenUDP(string IPaddr, int port)
        {
            udpListen = new Thread(() =>
            {
                udpServer = new UdpClient(portNum);
                Invoke(new Action(() =>
                {
                    consoleView.Text += "Listening to " + IPAddress.Any.ToString() + ":" + portNum + "\n";
                }));
                while (true)
                {
                    string fulltext = null;
                    var remoteEP = new IPEndPoint(IPAddress.Parse(IPaddr), port);
                    data = udpServer.Receive(ref remoteEP);
                    //data = Decompress(data);
                    for (int i = 0; i < data.Length; i++) fulltext += Convert.ToChar(data[i]);
                    if (nodeA)
                    {
                        if (AESexchange == false)
                        {
                            //MessageBox.Show("A receives RSA key from B here.");
                            Invoke(new Action(() =>
                            {
                                messageView.Text += "Received : " + fulltext + "\n";
                            }));
                            //MessageBox.Show("A sends AES key to B here.");
                            recipientKey = fulltext;
                            sendAESrsaKey(remoteEP.Address.ToString(), remoteEP.Port, Inputkey, recipientKey, AESexchange);
                            AESexchange = true;
                            Invoke(new Action(() =>
                            {
                                if(string2send!="")
                                    sendBtn.PerformClick(); 
                                else if(callBtn.Tag.Equals("CallOn"))//sender begin call after receiver sent rsa
                                    callBtn.PerformClick();
                            }));
                        }
                        else
                        {
                            //MessageBox.Show("A receives AES content here.");
                            Invoke(new Action(() =>
                            {
                                try
                                {
                                    string[] splitStr = AESEncDec.DecryptText(fulltext, Inputkey).Split('!');
                                    if (splitStr[2].Equals("text"))
                                    {
                                        messageView.Text += splitStr[0] + "\n";
                                    }
                                    else if (splitStr[2].Equals("call0"))
                                    {
                                        videoBox.BackColor = Color.FromName(splitStr[0]);
                                        consoleView.Text += "Receiving call data" + "\n";
                                    }
                                    else if (splitStr[2].Equals("call1"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("file0"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("file1"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("scr0"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("scr1"))
                                    {

                                    }
                                    else
                                    {
                                        consoleView.Text += "Unknown data received." + "\n";
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                inputText.Text += "";
                                connStatus.ForeColor = Color.Green;
                                connStatus.Text = "Connected";
                            }));
                        }
                    }
                    else if (nodeA == false && nodeB == false)
                    {
                        //MessageBox.Show("B receives RSA key from A and B sends key to A here.");
                        Invoke(new Action(() =>
                        {
                            messageView.Text += "Received : " + fulltext + "\n";
                        }));
                        recipientKey = fulltext;
                        Thread.Sleep(500);
                        Invoke(new Action(() =>
                        {
                            sendRSAkey(remoteEP.Address.ToString(), remoteEP.Port, publicKey, nodeB);
                        }));
                        nodeB = true;
                    }
                    else if (nodeB)
                    {
                        if (AESexchange == false)
                        {
                            //MessageBox.Show("B receives AES key here." + fulltext);
                            aesKey = RSAEncDec.DecryptText(fulltext, keySize, publicAndPrivateKey);
                            Invoke(new Action(() =>
                            {
                                messageView.Text += aesKey + "\n";
                            }));
                            AESexchange = true;
                        }
                        else
                        {
                            //MessageBox.Show("B receives AES content here.");
                            Invoke(new Action(() =>
                            {
                                try
                                {
                                    string[] splitStr = AESEncDec.DecryptText(fulltext, Inputkey).Split('!');
                                    if (splitStr[2].Equals("text"))
                                    {
                                        messageView.Text += splitStr[0] + "\n";
                                    }
                                    else if (splitStr[2].Equals("call0"))
                                    {
                                        if (!startCall)
                                        {
                                            //start receiver stopwatch
                                            //stopWatch.Start();                                            
                                            callBtn.PerformClick();
                                            startCall = true;
                                        }
                                        videoBox.BackColor = Color.FromName(splitStr[0]);
                                        callDuration.Text = stopWatch.Elapsed.ToString();
                                        consoleView.Text += "Receiving call data" + "\n";
                                    }
                                    else if (splitStr[2].Equals("call1"))
                                    {
                                        videoBox.BackColor = Color.FromName(splitStr[0]);
                                        consoleView.Text += "Not Receiving call data" + "\n";
                                        //stop receiver's stopwatch
                                        //if (stopWatch.IsRunning)
                                        //{
                                            //callBtn.PerformClick();
                                        //}

                                    }
                                    else if (splitStr[2].Equals("file0"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("file1"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("scr0"))
                                    {

                                    }
                                    else if (splitStr[2].Equals("scr1"))
                                    {

                                    }
                                    else
                                    {
                                        consoleView.Text += "Unknown data received." + "\n";
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                inputText.Text += "";
                                connStatus.ForeColor = Color.Green;
                                connStatus.Text = "Connected";
                            }));
                        }
                    }

                }
            });
            udpListen.Start();
            udpListen.IsBackground = true;
        }

        void sendRSAkey(string IPaddr, int port, string rsaKey, bool isItSent)
        {
            if (verifyXMLstring(rsaKey) && isItSent == false)
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(IPaddr), port);
                byte[] rsa2send = utf8.GetBytes(rsaKey);
                udpServer.Send(rsa2send, rsa2send.Length, remoteEP);
                isItSent = true;
                Invoke(new Action(() =>
                {
                    messageView.Text += "Sent : " + rsaKey + "\n";
                }));
            }
        }

        void sendAESrsaKey(string IPaddr, int port, string plainAesKey, string recipientRsaKey, bool isItSent)
        {
            if (verifyXMLstring(recipientRsaKey) && isItSent == false)
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(IPaddr), port);
                string aesKeyRsa = RSAEncDec.EncryptText(plainAesKey, keySize, recipientRsaKey);
                byte[] aes2send = utf8.GetBytes(aesKeyRsa);
                udpServer.Send(aes2send, aes2send.Length, remoteEP);
                isItSent = true;
                Invoke(new Action(() =>
                {
                    messageView.Text += plainAesKey + "\n";
                }));
            }
            else
            {
                MessageBox.Show("aes key not working.");
            }
        }

        void sendUDPmsg(string IPaddr, int port, string message)
        {
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(IPaddr), port);
            byte[] data2send;
            if (aesKey == null)
                data2send = utf8.GetBytes(AESEncDec.EncryptText(message, Inputkey));
            else
                data2send = utf8.GetBytes(AESEncDec.EncryptText(message, aesKey));
            udpServer.Send(data2send, data2send.Length, remoteEP);
        }

        void sendUDPfile(string IPaddr, int port, byte[] BytesFile)
        {
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(IPaddr), port);
            byte[] data2send;
            int sizeFile = BytesFile.Length;
            if (sizeFile == 0) MessageBox.Show("Uploaded file is empty or not valid.");
            else
            {
                if (aesKey == null)
                    data2send = AESEncDec.Encrypt(BytesFile, Inputkey);
                else
                    data2send = AESEncDec.Encrypt(BytesFile, aesKey);

                if (sizeFile > 400)
                {
                    double total = 0;
                    if (sizeFile % 400 == 0)
                    {
                        total = sizeFile / 400;
                        for (int j = 0; j < total; j++)
                        {
                            byte[] block = new byte[400];
                            Array.Copy(data2send, j, block, 0, 400);
                            udpServer.Send(block, block.Length, remoteEP);
                            Thread.Sleep(250);
                        }
                    }
                    else
                    {
                        total = Math.Ceiling((double)(sizeFile / 400));
                        for (int j = 0; j < total; j++)
                        {
                            if (j == total - 1)
                            {
                                byte[] block = new byte[sizeFile % 400];
                                Array.Copy(data2send, j, block, 0, sizeFile % 400);
                                udpServer.Send(block, block.Length, remoteEP);
                                Thread.Sleep(250);
                            }
                            else
                            {
                                byte[] block = new byte[400];
                                Array.Copy(data2send, j, block, 0, 400);
                                udpServer.Send(block, block.Length, remoteEP);
                                Thread.Sleep(250);
                            }
                        }
                    }
                    fileConsole.Text += "Sent " + total + " packets of file.\n";
                }
                else
                {
                    udpServer.Send(data2send, data2send.Length, remoteEP);
                    fileConsole.Text += "File has been sent.\n";
                }
            }
        }

        public void ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                byte[] myByte = new byte[byteArray.Length];
                MemoryStream theMemStream = new MemoryStream();
                theMemStream.Write(myByte, 0, myByte.Length);
                using (StreamWriter stream = new StreamWriter(fileName, false))
                {
                    stream.Write(byteArray);
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                consoleView.Text += "File receiving exception : " + ex.ToString();
            }
        }

        //async void listener(string IPaddr, int port, TcpListener listnr, Socket s)
        //{
        //    IPAddress ipAd = IPAddress.Parse(IPaddr);
        //    try
        //    {
        //        listnr = new TcpListener(ipAd, port);
        //        //encodes to ascii bytes to send reply to client            
        //        await Task.Delay(500);
        //        consoleView.Text += "Listening to " + listnr.LocalEndpoint + "\n";
        //        consoleView.Text += "Waiting for a connection...\n";
        //        await Task.Delay(500);

        //        listnr.Start();
        //        new Thread(() =>
        //        {

        //            Socket sockt; //= new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        //            try
        //            {
        //                while (true)
        //                {
        //                    sockt = listnr.AcceptSocket();
        //                    Invoke(new Action(() =>
        //                    {
        //                        myList = listnr;
        //                        socket = sockt;
        //                        consoleView.Text += "Connection accepted from " + sockt.RemoteEndPoint + "\n";
        //                    }));
        //                    byte[] b = new byte[100];
        //                    //receive streamed bytes and writes to bytearray b
        //                    int k = sockt.Receive(b);
        //                    Invoke(new Action(() =>
        //                    {
        //                        consoleView.Text += "Received " + b.Length + " B";
        //                    }));
        //                    string fullText = null;
        //                    for (int i = 0; i < k; i++) fullText += Convert.ToChar(b[i]);
        //                    //converts bytes to chars and write to console
        //                    Thread.Sleep(500);
        //                    sockt.Send(utf8.GetBytes("The string was received by the server.\n")); //send encoded bytes to client and close connection
        //                    Thread.Sleep(500);
        //                    string decryptedText = AESEncDec.DecryptText(fullText, Inputkey);
        //                    Invoke(new Action(() =>
        //                    {
        //                        messageView.Text += decryptedText + "\n";
        //                        consoleView.Text += " , Sent Acknowledgement\n";
        //                    }));
        //                }

        //            }
        //            catch (Exception expt)
        //            {
        //                Invoke(new Action(() =>
        //                {
        //                    consoleView.Text += "Socket unreachable\n";
        //                }));
        //                expt.ToString();
        //            }

        //        }).Start();

        //    }
        //    catch (Exception ex)
        //    {
        //        consoleView.Text += "Error..... " + ex.StackTrace;
        //    }

        //}

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        async void connect(string IPaddr, int port)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            connStatus.Text = "";
            connStatus.Text = "Connecting... \n";
            await Task.Delay(1000);
            IPAddress IPAd = IPAddress.Parse(IPaddr);
            try
            {
                TcpClient tcpclnt = new TcpClient();
                tcpclnt.Connect(IPAd, port); //connects
                connStatus.Text = "";
                connStatus.Text = "Connected";
                connStatus.ForeColor = Color.Blue;

                string str = nickName + " -> " + inputText.Text; //reads string from keyboard
                messageView.Text += str + "\n";
                string encryptedText = AESEncDec.EncryptText(str, Inputkey);
                Stream stm = tcpclnt.GetStream(); //returns the network stream - ready to write on stream
                byte[] ba = utf8.GetBytes(encryptedText);

                connStatus.Text = "";
                connStatus.Text = "Transmitting...";

                consoleView.Text += "Sent " + ba.Length + " B\n";
                connStatus.Text = "";
                connStatus.Text = "Connected";
                stm.Write(ba, 0, ba.Length); //writes bytes array to stream, from first until last byte
                consoleView.Text += encryptedText + "\n";
                await Task.Delay(500);

                byte[] bb = new byte[1024]; //creates new array of byte elements -- byte[] bb = new byte[100]
                int k = stm.Read(bb, 0, bb.Length); //reads stream answer into 100 bytes bytearray
                for (int i = 0; i < k; i++)
                    consoleView.Text += Convert.ToChar(bb[i]); //convert to char (may need to get the string if read the chars) - return incoming message
                tcpclnt.Close();
                inputText.Text = "";
            }
            catch (Exception ex)
            {
                consoleView.Text += "Error... " + ex.StackTrace.ToString() + "\n";
            }

        }


        private void inputText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sendBtn_Click(this, new EventArgs());
            }
        }


        private void initBtn_Click(object sender, EventArgs e)
        {

        }

        public string GetLanIpAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (!nic.Name.ToString().Contains("Virtual"))
                {
                    if (nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        foreach (UnicastIPAddressInformation ip in nic.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ip.Address))
                            {
                                return ip.Address.ToString();
                            }
                        }
                    }
                }
            }
            return null;
        }  

        string GetWanIpAddress()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        public bool IsPrivate(string ipAddress)
        {
            int[] ipParts = ipAddress.Split(new String[] { "." }, StringSplitOptions.RemoveEmptyEntries)
                                     .Select(s => int.Parse(s)).ToArray();
            // in private ip range
            if (ipParts[0] == 10 ||
                (ipParts[0] == 192 && ipParts[1] == 168) ||
                (ipParts[0] == 172 && (ipParts[1] >= 16 && ipParts[1] <= 31)))
            {
                return true;
            }
            return false;
        }

        void AddFirewallRule()
        {
            Guid netFwRuleUuid = new Guid("{2C5BC43E-3369-4C33-AB0C-BE9469677AF4}");
            INetFwRule rule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromCLSID(netFwRuleUuid));

            rule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
            rule.ApplicationName = @"%userprofile%\Documents\Visual Studio 2015\Projects\socketClient\socketClient\bin\Debug\socketClient.exe";
            rule.Description = "It allows P2P Chat to function using NAT Traversal mechanism.";
            rule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
            rule.EdgeTraversal = true;
            rule.Enabled = true;
            rule.Grouping = "P2P_CHAT_GROUP";
            rule.Name = "P2PChat_NatEnabled";
            rule.Protocol = (int)ProtocolType.Tcp;

            Guid netFwPolicy2Uuid = new Guid("{E2B3C97F-6AE1-41AC-817A-F6F92166D7DD}");
            INetFwPolicy2 policy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromCLSID(netFwPolicy2Uuid));
            policy.Rules.Add(rule);
        }

        private void consoleView_TextChanged(object sender, EventArgs e)
        {
            consoleView.SelectionStart = consoleView.Text.Length;
            consoleView.ScrollToCaret();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse Text Files";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;
                fileConsole.Text = "";
                fileConsole.Text += "File for uploading : " + openFileDialog1.FileName + "\n";
                try
                {
                    using (StreamReader sr = new StreamReader(fileName))
                    {
                        fileBytes = File.ReadAllBytes(fileName);
                        sr.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }

            }
            else
            {
                if (fileName != null) openFileDialog1.InitialDirectory = fileName;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (validIPandPort())
            {
                if (AESexchange == true && (nodeA == true || nodeB == true))
                {
                    sendUDPfile(ipBox.Text, int.Parse(portBox.Text), fileBytes);
                    Invoke(new Action(() =>
                    {
                        fileConsole.Text += "File sent.";
                        connStatus.ForeColor = Color.Green;
                    }));
                }
                else
                {
                    sendRSAkey(ipBox.Text, int.Parse(portBox.Text), publicKey, nodeA);
                    nodeA = true;
                }
            }

        }

        private void App_Load(object sender, EventArgs e)
        {
            formIntro.Hide();
            RSAEncDec.GenerateKeys(keySize, out publicKey, out publicAndPrivateKey);
            Random rand = new Random();
            portNum = rand.Next(12000, 12010);
            string plain = Inputkey;
            //listener(IPAddress.Any.ToString(), portNum, myList, socket);
            listenUDP(IPAddress.Any.ToString(), portNum);
            

            connStatus.Text = "Not Connected";
            try
            {
                consoleView.Text = "LAN IP : " + GetLanIpAddress() + "\n";
            }
            finally
            {

            }
            AcceptButton = sendBtn; //ctrl+enter
            ipThread = new Thread(() =>
            {
                string wanIPinfo = null;
                wanIPinfo = GetWanIpAddress();
                Invoke(new Action(() =>
                {
                    consoleView.Text += "WAN IP : " + wanIPinfo + "\n";
                }));
            });
            ipThread.Start();
            ipThread.IsBackground = true;
            LoadCamList();
            LoadMicList();
            LoadSpeakersList();
        }

        private void proxySW_CheckedChanged(object sender, EventArgs e)
        {
            if (proxySW.Checked)
                EnableProxy(true);
            else
                EnableProxy(false);
        }

        

        void LoadCamList()
        {
            try
            {
                webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo VideoCaptureDevice in webcam)
                {
                    camList.Items.Add(VideoCaptureDevice.Name);
                }
                if (camList.Items.Count > 0) camList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void LoadMicList()
        {
            try
            {
                int waveInDevices = WaveIn.DeviceCount;
                for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
                {
                    WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                    micList.Items.Add(deviceInfo.ProductName);
                    //("Device {0}: {1}, {2} channels", waveInDevice, deviceInfo.ProductName, deviceInfo.Channels);
                }
                if (waveInDevices > 0) micList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void LoadSpeakersList()
        {
            try
            {
                int waveOutDevices = WaveOut.DeviceCount;
                for (int waveOutDevice = 0; waveOutDevice < waveOutDevices; waveOutDevice++)
                {
                    WaveOutCapabilities deviceInfo = WaveOut.GetCapabilities(waveOutDevice);
                    speakersList.Items.Add(deviceInfo.ProductName);
                }
                if (waveOutDevices > 0) speakersList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void EnableProxy(bool state)
        {
            proxyType1.Enabled = state;
            proxyType2.Enabled = state;
            proxyType3.Enabled = state;
            label11.Enabled = state;
            label12.Enabled = state;
            proxyIP.Enabled = state;
            proxyPort.Enabled = state;
            shareScreen.Enabled = state;
            proxyEnabled = state;
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            
            if (validIPandPort())
            {
                string2send = nickName + " --> " + inputText.Text ;//string2send defined here - fix second click bug on first msg sent
                if (AESexchange == true && (nodeA == true || nodeB == true))
                {
                    sendUDPmsg(ipBox.Text, int.Parse(portBox.Text), string2send + "!" + inputText.Text.Length + "!" + "text");
                    Invoke(new Action(() =>
                    {
                        inputText.Text = "";
                        connStatus.Text = "Connected";
                        connStatus.ForeColor = Color.Green;
                        messageView.Text += string2send + "\n";
                    }));
                }
                else
                {
                    sendRSAkey(ipBox.Text, int.Parse(portBox.Text), publicKey, nodeA);
                    nodeA = true;
                }
            }
            
        }

        void initCall(object sender, EventArgs e)
        {
            callThr = new Thread(() =>
            {
                try
                {
                    BeginCall(sender, e);
                    Invoke(new Action(() =>
                    {
                        inputText.Text = "";
                        connStatus.Text = "Connected";
                        connStatus.ForeColor = Color.Green;
                        stopWatch.Start();
                    }));
                    while (true)
                        Invoke(new Action(() =>
                        {
                            callDuration.Text = stopWatch.Elapsed.ToString();
                        }));

                }
                catch (Exception exc)
                {
                    EndCall(sender, e);
                    stopWatch.Stop();
                    stopWatch.Reset();
                    Invoke(new Action(() =>
                    {
                        videoBox.BackColor = Color.Black;
                        consoleView.Text += "Not receiving call" + "\n";
                    }));
                }
            });
            callThr.IsBackground = true;
        }

        private void callBtn_Click(object sender, EventArgs e)
        {
            if (AESexchange == true && (nodeA == true || nodeB == true)) //exchanged rsa keys
            {
                initCall(sender, e);//init thread
                if (callBtn.Tag.Equals("CallOff"))
                {
                    EnableCallIcons(true);
                    callThr.Start();
                }
                else
                {
                    EnableCallIcons(false);
                    callThr.Abort();
                }
            }                
            else
            {
                if (validIPandPort())
                {
                    sendRSAkey(ipBox.Text, int.Parse(portBox.Text), publicKey, nodeA);
                    nodeA = true;
                }
            }


            if (callBtn.Tag.Equals("CallOff"))
            {
                callBtn.Tag = "CallOn";
            }
            else
            {
                callBtn.Tag = "CallOff";
            }

        }

        void EnableCallIcons(bool state)
        {
            callDuration.Visible = state;
            camBtn.Visible = state;
            micBtn.Visible = state;
        }

        public void BeginCall(object sender, EventArgs e)
        {
            sourceStream = new WaveInEvent
            {
                DeviceNumber = micList.SelectedIndex,
                WaveFormat = new WaveFormat(44100, WaveIn.GetCapabilities(micList.SelectedIndex).Channels)
            };
            sourceStream.DataAvailable += StreamDataAvailable;
            //Write to file, testing only - remove on published version //
            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }
            waveWriter = new WaveFileWriter(FilePath + FileName, sourceStream.WaveFormat);
            //End test file writing//
            sourceStream.StartRecording();
        }

        
        public void StreamDataAvailable(object sender, WaveInEventArgs e)
        {
            if (waveWriter != null)
            {
                byte[] cipherMsg = AESEncDec.Encrypt(e.Buffer, Inputkey); ;
                try
                {                    
                    waveWriter.Write(cipherMsg, 0, cipherMsg.Length);
                    waveWriter.Flush();
                    Invoke(new Action(() =>
                    {
                        sendUDPmsg(ipBox.Text, int.Parse(portBox.Text), "red"+"!" + cipherMsg.Length.ToString() + "!call0");
                        messageView.Text += " !" + cipherMsg.Length.ToString() + "!call" + "\n"; //sending test msg for call - remove on release
                    }));
                    //sendUDPfile(ipBox.Text, int.Parse(portBox.Text), cipherMsg);
                }
                catch (Exception ex)
                {
                    Invoke(new Action(() =>
                    {
                        sendUDPmsg(ipBox.Text, int.Parse(portBox.Text), "black" + "!" + cipherMsg.Length.ToString() + "!call1");
                        consoleView.Text += "Audio not streaming." + "\n";
                    }));
                }
            }
        }

        private void EndCall(object sender, EventArgs e)
        {
            if (sourceStream != null)
            {
                sourceStream.StopRecording();
                sourceStream.Dispose();
                sourceStream = null;
            }
            if (waveWriter != null)
            {
                waveWriter.Dispose();
                waveWriter = null;
            }
        }

        public byte[] ImageToByte(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        private void inputText_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inputText.Text))
            {
                sendBtn.Enabled = true;
            }
            else
            {
                sendBtn.Enabled = false;
            }
        }

        private void messageView_TextChanged(object sender, EventArgs e)
        {   
            messageView.SelectionStart = messageView.Text.Length;
            messageView.ScrollToCaret();            
        }


        private void callDuration_Click(object sender, EventArgs e)
        {

        }

        private void camBtn_Click(object sender, EventArgs e)
        {

        }

        private void videoBox_Click(object sender, EventArgs e)
        {

        }

        private void micBtn_Click(object sender, EventArgs e)
        {

        }                

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Move_Form(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

    }
}
