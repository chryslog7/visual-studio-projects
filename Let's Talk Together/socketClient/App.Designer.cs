﻿namespace p2pchat
{
    partial class App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(App));
            this.label1 = new System.Windows.Forms.Label();
            this.fileUpload = new System.Windows.Forms.OpenFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.fileStatus = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.fileConsole = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.callBtn = new System.Windows.Forms.Button();
            this.callDuration = new System.Windows.Forms.Label();
            this.micBtn = new System.Windows.Forms.Button();
            this.camBtn = new System.Windows.Forms.Button();
            this.videoBox = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.sendBtn = new System.Windows.Forms.Button();
            this.messageView = new System.Windows.Forms.RichTextBox();
            this.connStatus = new System.Windows.Forms.Label();
            this.inputText = new System.Windows.Forms.RichTextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.proxyPort = new System.Windows.Forms.TextBox();
            this.proxyIP = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.proxyType3 = new System.Windows.Forms.RadioButton();
            this.proxyType2 = new System.Windows.Forms.RadioButton();
            this.proxyType1 = new System.Windows.Forms.RadioButton();
            this.proxySW = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.consoleView = new System.Windows.Forms.RichTextBox();
            this.portBox = new System.Windows.Forms.TextBox();
            this.ipBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.shareScreen = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.speakersList = new System.Windows.Forms.ComboBox();
            this.micList = new System.Windows.Forms.ComboBox();
            this.camList = new System.Windows.Forms.ComboBox();
            this.logo = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.callTimer = new System.Windows.Forms.Timer(this.components);
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shareScreen)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Calibri", 22F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(319, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 34);
            this.label6.TabIndex = 10;
            this.label6.Text = "Let\'s Talk Together";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.fileStatus);
            this.tabPage4.Controls.Add(this.button4);
            this.tabPage4.Controls.Add(this.button3);
            this.tabPage4.Controls.Add(this.fileConsole);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(739, 330);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "File Transfer";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // fileStatus
            // 
            this.fileStatus.AutoSize = true;
            this.fileStatus.Location = new System.Drawing.Point(6, 280);
            this.fileStatus.Name = "fileStatus";
            this.fileStatus.Size = new System.Drawing.Size(0, 19);
            this.fileStatus.TabIndex = 8;
            this.fileStatus.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.BackgroundImage = global::p2pchat.Properties.Resources.secure_file_upload;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.Location = new System.Drawing.Point(627, 265);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(48, 48);
            this.button4.TabIndex = 7;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.BackgroundImage = global::p2pchat.Properties.Resources.send_secure_file;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Location = new System.Drawing.Point(681, 265);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(48, 48);
            this.button3.TabIndex = 6;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // fileConsole
            // 
            this.fileConsole.BackColor = System.Drawing.SystemColors.ControlLight;
            this.fileConsole.Location = new System.Drawing.Point(10, 17);
            this.fileConsole.Name = "fileConsole";
            this.fileConsole.ReadOnly = true;
            this.fileConsole.Size = new System.Drawing.Size(719, 236);
            this.fileConsole.TabIndex = 5;
            this.fileConsole.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.callBtn);
            this.tabPage3.Controls.Add(this.callDuration);
            this.tabPage3.Controls.Add(this.micBtn);
            this.tabPage3.Controls.Add(this.camBtn);
            this.tabPage3.Controls.Add(this.videoBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(739, 330);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Call";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // callBtn
            // 
            this.callBtn.BackColor = System.Drawing.Color.White;
            this.callBtn.BackgroundImage = global::p2pchat.Properties.Resources.call_button;
            this.callBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.callBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.callBtn.FlatAppearance.BorderSize = 0;
            this.callBtn.Location = new System.Drawing.Point(681, 265);
            this.callBtn.Name = "callBtn";
            this.callBtn.Size = new System.Drawing.Size(48, 48);
            this.callBtn.TabIndex = 0;
            this.callBtn.Tag = "CallOff";
            this.callBtn.UseVisualStyleBackColor = false;
            this.callBtn.Click += new System.EventHandler(this.callBtn_Click);
            // 
            // callDuration
            // 
            this.callDuration.Font = new System.Drawing.Font("Times New Roman", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.callDuration.Location = new System.Drawing.Point(332, 280);
            this.callDuration.Name = "callDuration";
            this.callDuration.Size = new System.Drawing.Size(75, 19);
            this.callDuration.TabIndex = 4;
            this.callDuration.Text = "00:00";
            this.callDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.callDuration.Visible = false;
            this.callDuration.Click += new System.EventHandler(this.callDuration_Click);
            // 
            // micBtn
            // 
            this.micBtn.BackColor = System.Drawing.Color.White;
            this.micBtn.BackgroundImage = global::p2pchat.Properties.Resources.mic_on;
            this.micBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.micBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.micBtn.FlatAppearance.BorderSize = 0;
            this.micBtn.Location = new System.Drawing.Point(64, 265);
            this.micBtn.Name = "micBtn";
            this.micBtn.Size = new System.Drawing.Size(48, 48);
            this.micBtn.TabIndex = 3;
            this.micBtn.Tag = "MicOn";
            this.micBtn.UseVisualStyleBackColor = false;
            this.micBtn.Visible = false;
            this.micBtn.Click += new System.EventHandler(this.micBtn_Click);
            // 
            // camBtn
            // 
            this.camBtn.BackColor = System.Drawing.Color.White;
            this.camBtn.BackgroundImage = global::p2pchat.Properties.Resources.video_on;
            this.camBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.camBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.camBtn.FlatAppearance.BorderSize = 0;
            this.camBtn.Location = new System.Drawing.Point(10, 265);
            this.camBtn.Name = "camBtn";
            this.camBtn.Size = new System.Drawing.Size(48, 48);
            this.camBtn.TabIndex = 2;
            this.camBtn.Tag = "VideoOn";
            this.camBtn.UseVisualStyleBackColor = false;
            this.camBtn.Visible = false;
            this.camBtn.Click += new System.EventHandler(this.camBtn_Click);
            // 
            // videoBox
            // 
            this.videoBox.BackColor = System.Drawing.Color.Black;
            this.videoBox.Location = new System.Drawing.Point(10, 17);
            this.videoBox.Name = "videoBox";
            this.videoBox.Size = new System.Drawing.Size(719, 242);
            this.videoBox.TabIndex = 1;
            this.videoBox.TabStop = false;
            this.videoBox.Click += new System.EventHandler(this.videoBox_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.sendBtn);
            this.tabPage2.Controls.Add(this.messageView);
            this.tabPage2.Controls.Add(this.connStatus);
            this.tabPage2.Controls.Add(this.inputText);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(739, 330);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Chat";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // sendBtn
            // 
            this.sendBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sendBtn.BackColor = System.Drawing.Color.White;
            this.sendBtn.BackgroundImage = global::p2pchat.Properties.Resources.send_pm;
            this.sendBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sendBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendBtn.Enabled = false;
            this.sendBtn.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.sendBtn.FlatAppearance.BorderSize = 0;
            this.sendBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.sendBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sendBtn.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.sendBtn.Location = new System.Drawing.Point(699, 271);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(34, 30);
            this.sendBtn.TabIndex = 0;
            this.sendBtn.Tag = "SendOff";
            this.sendBtn.UseVisualStyleBackColor = false;
            this.sendBtn.Click += new System.EventHandler(this.sendBtn_Click);
            // 
            // messageView
            // 
            this.messageView.Location = new System.Drawing.Point(6, 6);
            this.messageView.Name = "messageView";
            this.messageView.ReadOnly = true;
            this.messageView.Size = new System.Drawing.Size(727, 146);
            this.messageView.TabIndex = 4;
            this.messageView.Text = "";
            this.messageView.TextChanged += new System.EventHandler(this.messageView_TextChanged);
            // 
            // connStatus
            // 
            this.connStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.connStatus.AutoSize = true;
            this.connStatus.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.connStatus.ForeColor = System.Drawing.Color.Red;
            this.connStatus.Location = new System.Drawing.Point(628, 304);
            this.connStatus.Name = "connStatus";
            this.connStatus.Size = new System.Drawing.Size(105, 20);
            this.connStatus.TabIndex = 10;
            this.connStatus.Text = "Not Connected";
            this.connStatus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // inputText
            // 
            this.inputText.Location = new System.Drawing.Point(6, 158);
            this.inputText.Name = "inputText";
            this.inputText.Size = new System.Drawing.Size(727, 143);
            this.inputText.TabIndex = 5;
            this.inputText.Text = "";
            this.inputText.TextChanged += new System.EventHandler(this.inputText_TextChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.proxyPort);
            this.tabPage1.Controls.Add(this.proxyIP);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.proxyType3);
            this.tabPage1.Controls.Add(this.proxyType2);
            this.tabPage1.Controls.Add(this.proxyType1);
            this.tabPage1.Controls.Add(this.proxySW);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.consoleView);
            this.tabPage1.Controls.Add(this.portBox);
            this.tabPage1.Controls.Add(this.ipBox);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(739, 330);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Network";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label11.Location = new System.Drawing.Point(230, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 19);
            this.label11.TabIndex = 32;
            this.label11.Text = "Port";
            // 
            // proxyPort
            // 
            this.proxyPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.proxyPort.Enabled = false;
            this.proxyPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.proxyPort.Location = new System.Drawing.Point(234, 237);
            this.proxyPort.Name = "proxyPort";
            this.proxyPort.Size = new System.Drawing.Size(58, 23);
            this.proxyPort.TabIndex = 30;
            // 
            // proxyIP
            // 
            this.proxyIP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.proxyIP.Enabled = false;
            this.proxyIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.proxyIP.Location = new System.Drawing.Point(25, 237);
            this.proxyIP.Name = "proxyIP";
            this.proxyIP.Size = new System.Drawing.Size(196, 23);
            this.proxyIP.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12F);
            this.label12.Location = new System.Drawing.Point(21, 207);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 19);
            this.label12.TabIndex = 31;
            this.label12.Text = "IPv4 Address";
            // 
            // proxyType3
            // 
            this.proxyType3.AutoSize = true;
            this.proxyType3.Enabled = false;
            this.proxyType3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic);
            this.proxyType3.Location = new System.Drawing.Point(195, 175);
            this.proxyType3.Name = "proxyType3";
            this.proxyType3.Size = new System.Drawing.Size(80, 22);
            this.proxyType3.TabIndex = 28;
            this.proxyType3.TabStop = true;
            this.proxyType3.Text = "SOCKSv5";
            this.proxyType3.UseVisualStyleBackColor = true;
            // 
            // proxyType2
            // 
            this.proxyType2.AutoSize = true;
            this.proxyType2.Enabled = false;
            this.proxyType2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic);
            this.proxyType2.Location = new System.Drawing.Point(97, 175);
            this.proxyType2.Name = "proxyType2";
            this.proxyType2.Size = new System.Drawing.Size(80, 22);
            this.proxyType2.TabIndex = 27;
            this.proxyType2.TabStop = true;
            this.proxyType2.Text = "SOCKSv4";
            this.proxyType2.UseVisualStyleBackColor = true;
            // 
            // proxyType1
            // 
            this.proxyType1.AutoSize = true;
            this.proxyType1.Enabled = false;
            this.proxyType1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic);
            this.proxyType1.Location = new System.Drawing.Point(25, 175);
            this.proxyType1.Name = "proxyType1";
            this.proxyType1.Size = new System.Drawing.Size(57, 22);
            this.proxyType1.TabIndex = 26;
            this.proxyType1.TabStop = true;
            this.proxyType1.Text = "HTTP";
            this.proxyType1.UseVisualStyleBackColor = true;
            // 
            // proxySW
            // 
            this.proxySW.AutoSize = true;
            this.proxySW.Location = new System.Drawing.Point(80, 146);
            this.proxySW.Name = "proxySW";
            this.proxySW.Size = new System.Drawing.Size(15, 14);
            this.proxySW.TabIndex = 25;
            this.proxySW.UseVisualStyleBackColor = true;
            this.proxySW.CheckedChanged += new System.EventHandler(this.proxySW_CheckedChanged);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label10.Location = new System.Drawing.Point(21, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 24);
            this.label10.TabIndex = 24;
            this.label10.Text = "Proxy";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(21, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 24);
            this.label5.TabIndex = 23;
            this.label5.Text = "Connection";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(230, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 19);
            this.label4.TabIndex = 14;
            this.label4.Text = "Port";
            // 
            // consoleView
            // 
            this.consoleView.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.consoleView.Location = new System.Drawing.Point(369, 0);
            this.consoleView.Name = "consoleView";
            this.consoleView.ReadOnly = true;
            this.consoleView.Size = new System.Drawing.Size(370, 330);
            this.consoleView.TabIndex = 1;
            this.consoleView.Text = "";
            this.consoleView.TextChanged += new System.EventHandler(this.consoleView_TextChanged);
            // 
            // portBox
            // 
            this.portBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.portBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.portBox.Location = new System.Drawing.Point(234, 89);
            this.portBox.Name = "portBox";
            this.portBox.Size = new System.Drawing.Size(58, 23);
            this.portBox.TabIndex = 12;
            // 
            // ipBox
            // 
            this.ipBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ipBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.ipBox.Location = new System.Drawing.Point(25, 89);
            this.ipBox.Name = "ipBox";
            this.ipBox.Size = new System.Drawing.Size(196, 23);
            this.ipBox.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(21, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 19);
            this.label3.TabIndex = 13;
            this.label3.Text = "IPv4 Address";
            // 
            // tabs
            // 
            this.tabs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabs.Controls.Add(this.tabPage1);
            this.tabs.Controls.Add(this.tabPage2);
            this.tabs.Controls.Add(this.tabPage3);
            this.tabs.Controls.Add(this.tabPage4);
            this.tabs.Controls.Add(this.tabPage5);
            this.tabs.Controls.Add(this.tabPage6);
            this.tabs.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.tabs.Location = new System.Drawing.Point(17, 55);
            this.tabs.MinimumSize = new System.Drawing.Size(747, 362);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(747, 362);
            this.tabs.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabs.TabIndex = 5;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.shareScreen);
            this.tabPage5.Location = new System.Drawing.Point(4, 28);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(739, 330);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Share Screen";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // shareScreen
            // 
            this.shareScreen.BackColor = System.Drawing.Color.Black;
            this.shareScreen.Location = new System.Drawing.Point(4, 6);
            this.shareScreen.Name = "shareScreen";
            this.shareScreen.Size = new System.Drawing.Size(730, 318);
            this.shareScreen.TabIndex = 2;
            this.shareScreen.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label9);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.label7);
            this.tabPage6.Controls.Add(this.label2);
            this.tabPage6.Controls.Add(this.speakersList);
            this.tabPage6.Controls.Add(this.micList);
            this.tabPage6.Controls.Add(this.camList);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(739, 330);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Settings";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label9.Location = new System.Drawing.Point(61, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 19);
            this.label9.TabIndex = 33;
            this.label9.Text = "Audio :";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label8.Location = new System.Drawing.Point(21, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 19);
            this.label8.TabIndex = 32;
            this.label8.Text = "Microphone :";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label7.Location = new System.Drawing.Point(48, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 19);
            this.label7.TabIndex = 31;
            this.label7.Text = "Camera :";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(23, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 24);
            this.label2.TabIndex = 30;
            this.label2.Text = "Input Devices";
            // 
            // speakersList
            // 
            this.speakersList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.speakersList.FormattingEnabled = true;
            this.speakersList.Location = new System.Drawing.Point(129, 146);
            this.speakersList.Name = "speakersList";
            this.speakersList.Size = new System.Drawing.Size(165, 27);
            this.speakersList.TabIndex = 29;
            // 
            // micList
            // 
            this.micList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.micList.FormattingEnabled = true;
            this.micList.Location = new System.Drawing.Point(129, 105);
            this.micList.Name = "micList";
            this.micList.Size = new System.Drawing.Size(165, 27);
            this.micList.TabIndex = 28;
            // 
            // camList
            // 
            this.camList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.camList.FormattingEnabled = true;
            this.camList.Location = new System.Drawing.Point(129, 63);
            this.camList.Name = "camList";
            this.camList.Size = new System.Drawing.Size(165, 27);
            this.camList.TabIndex = 27;
            // 
            // logo
            // 
            this.logo.Cursor = System.Windows.Forms.Cursors.Default;
            this.logo.Image = global::p2pchat.Properties.Resources.lets_talk_together_logo;
            this.logo.Location = new System.Drawing.Point(277, 16);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(36, 36);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 11;
            this.logo.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::p2pchat.Properties.Resources.switch_off_button;
            this.pictureBox1.Location = new System.Drawing.Point(720, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Move_Form);
            // 
            // callTimer
            // 
            this.callTimer.Interval = 1000;
            // 
            // App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(780, 430);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(780, 430);
            this.Name = "App";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Let\'s Talk Together - Encrypted communication";
            this.Load += new System.EventHandler(this.App_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Move_Form);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabs.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shareScreen)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog fileUpload;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RichTextBox fileConsole;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button callBtn;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox messageView;
        private System.Windows.Forms.RichTextBox inputText;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.Label connStatus;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox consoleView;
        private System.Windows.Forms.TextBox portBox;
        private System.Windows.Forms.TextBox ipBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.PictureBox videoBox;
        private System.Windows.Forms.Button micBtn;
        private System.Windows.Forms.Button camBtn;
        private System.Windows.Forms.Label callDuration;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label fileStatus;
        private System.Windows.Forms.Timer callTimer;
        private System.Windows.Forms.CheckBox proxySW;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox speakersList;
        private System.Windows.Forms.ComboBox micList;
        private System.Windows.Forms.ComboBox camList;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox proxyPort;
        private System.Windows.Forms.TextBox proxyIP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton proxyType3;
        private System.Windows.Forms.RadioButton proxyType2;
        private System.Windows.Forms.RadioButton proxyType1;
        private System.Windows.Forms.PictureBox shareScreen;
    }
}

