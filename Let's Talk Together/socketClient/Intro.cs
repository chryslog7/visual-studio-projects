﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace p2pchat
{
    public partial class Intro : Form
    {
        public App formApp;
        public Intro()
        {
            InitializeComponent();
        }

        private void Intro_Load(object sender, EventArgs e)
        {
            formApp = new App(this);
        }

        private void nickname_TextChanged(object sender, EventArgs e)
        {
            if (nickname.Text.Length > 0 && !string.IsNullOrWhiteSpace(nickname.Text) && Regex.IsMatch(nickname.Text, "^[a-zA-Z]"))
            {
                initBtn.Enabled = true;
            }
            else
            {
                initBtn.Enabled = false;
            }
        }

        private void initBtn_Click(object sender, EventArgs e)
        {
            formApp.nickName = nickname.Text;
            formApp.Show();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
