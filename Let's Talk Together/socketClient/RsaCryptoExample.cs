﻿using System;
using System.Security.Cryptography;

namespace p2pchat
{
    public class RSACryptor
    {
        public static string serialPubKey(RSAParameters deserPubKey) {
            var sw = new System.IO.StringWriter();
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, deserPubKey);
            return sw.ToString();
        }
        
        public static string deSerialPubKey(string serPubKey)
        {
            var sr = new System.IO.StringReader(serPubKey);
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            return xs.Deserialize(sr).ToString();            
        }

        public static string RSAencrypt(RSAParameters pubKey,string plainTextData)
        {
            var csp = new RSACryptoServiceProvider(2048);
            csp.ImportParameters(pubKey);
            var bytesPlainTextData = System.Text.Encoding.Unicode.GetBytes(plainTextData);
            var bytesCypherText = csp.Encrypt(bytesPlainTextData, false);
            var cipherText = Convert.ToBase64String(bytesCypherText);
            return cipherText;
        }

        public static string RSAdecrypt(RSAParameters privKey,string cipherText)
        {
            var bytesCypherText = Convert.FromBase64String(cipherText);
            var csp = new RSACryptoServiceProvider();
            csp.ImportParameters(privKey);
            var bytesPlainTextData = csp.Decrypt(bytesCypherText, false);
            var plainTextData = System.Text.Encoding.Unicode.GetString(bytesPlainTextData);
            return plainTextData;
        }

    }
}